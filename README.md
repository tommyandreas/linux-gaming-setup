# Fedora Gaming Setup

This is a step by step GUI guide to setup fedora for gaming. The goal is to provide a clear way to set your fedora installation for gaming via GUI, and avoid having to type any command inside the terminal.
